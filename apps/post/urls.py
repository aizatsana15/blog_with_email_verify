from django.urls import path
from apps.post.views import PostDetailAPIView, PostListCreateAPIView, CreateLikeDislikeAPIView

app_name = 'post'

urlpatterns = [
    path('', PostListCreateAPIView.as_view(), name='post-create-list'),
    path('<int:pk>/', PostDetailAPIView.as_view(), name='post-detail'),
    path('<int:pk>/like/', CreateLikeDislikeAPIView.as_view(), name='post-like'),
]
