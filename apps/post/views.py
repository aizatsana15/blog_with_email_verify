from rest_framework.generics import CreateAPIView, RetrieveAPIView, ListCreateAPIView
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED

from post.models import Post, Like
from post.serializers import PostSerializer, LikeDislikeSerializer


__all__ = [
    'PostListCreateAPIView',
    'PostDetailAPIView',
    'CreateLikeDislikeAPIView'
]


class PostListCreateAPIView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = PostSerializer
    queryset = Post.objects.all()


class PostDetailAPIView(RetrieveAPIView):
    permission_classes = (AllowAny, )
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class CreateLikeDislikeAPIView(CreateAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = LikeDislikeSerializer
    queryset = Like.objects.all()

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        like_obj, _ = Like.objects.get_or_create(post_id=kwargs['pk'], user=request.user)

        if like_obj.kind == data['kind']:
            return Response(status=HTTP_200_OK)

        like_obj.kind = data['kind']
        like_obj.save()
        return Response(status=HTTP_201_CREATED)
