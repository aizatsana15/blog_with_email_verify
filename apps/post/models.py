from django.db import models
from django.contrib.auth.models import User


__all__ = [
    'Post',
    'Like'
]


class Post(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='author')
    title = models.CharField(max_length=120, verbose_name='title')
    text = models.TextField(verbose_name='text')
    dt_created = models.DateTimeField(auto_now_add=True, verbose_name='date created')
    dt_updated = models.DateTimeField(auto_now=True, verbose_name='date updated')

    def __str__(self):
        return self.title

    @property
    def total_likes(self):
        return self.likes.filter(kind=Like.LIKE).count()

    @property
    def total_dislikes(self):
        return self.likes.filter(kind=Like.DISLIKE).count()


class Like(models.Model):
    LIKE = 1
    DISLIKE = -1

    KINDS = (
        (DISLIKE, 'Dislike'),
        (LIKE, 'Like')
    )

    kind = models.SmallIntegerField(verbose_name='kind', choices=KINDS, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='user')
    post = models.ForeignKey(Post, on_delete=models.CASCADE, verbose_name='post', related_name='likes')

    class Meta:
        unique_together = ('user', 'post',)
