from rest_framework.test import APITestCase
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status

import mock
from model_bakery import baker

from post.models import Post, Like


def mocked_check_email_by_hunter(email):
    return {
        'score': 99
    }


class PostTests(APITestCase):

    def setUp(self):
        self.test_user = baker.make(
            User, email='test.user@email.io', password=make_password('2020TestPassword!'), username='test_username',
        )
        self.test_post = baker.make(Post)

        self.access_token = self.client.post(
            reverse('jwt-create'),
            data={'username': 'test_username', 'password': '2020TestPassword!'},
            format='json'
        ).data['access']

    def test_get_all_post(self):
        """
        Ensure we get 200 response from API which return all posts.
        """
        url = reverse('post:post-create-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_post_detail_success(self):
        """
        Ensure we get 200 response from API which return post detail
        """
        url = reverse('post:post-detail', args=[self.test_post.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_post_detail_failed(self):
        """
        Ensure we get 404 response from API which return post detail for non exists post.
        """
        url = reverse('post:post-detail', args=[1000])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_like_success(self):
        """
        Ensure we get 201 response from API which create post like
        """
        url = reverse('post:post-like', args=[self.test_post.pk])
        payload = {'kind': Like.LIKE}
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.access_token)
        response = self.client.post(url, data=payload)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_post_failed(self):
        """
        Ensure we get 401 response from API which crete post with unauthorized user.
        """
        url = reverse('post:post-create-list')
        payload = {'title': 'Test title', 'text': 'Test post text'}
        response = self.client.post(url, data=payload)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_access_token_success(self):
        """
        Ensure we get 200 response with access token
        """
        response = self.client.post(
            reverse('jwt-create'),
            data={'username': self.test_user.username, 'password': '2020TestPassword!'},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('access' in response.data)

    @mock.patch('apps.core.utils.check_email_by_hunter', side_effect=mocked_check_email_by_hunter)
    def test_user_registration_success(self, check_email_by_hunter):
        """
        Ensure we get 201 response from Auth API which register new user.
        """
        url = reverse('user-list')
        payload = {'email': 'test.user_2@email.io', 'username': 'test_username_2', 'password': '2020TestPassword!'}

        # Stop including any credentials
        self.client.credentials()

        response = self.client.post(url, data=payload)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_post_success(self):
        """
        Ensure we get 201 response for create new post by authenticated user
        """
        url = reverse('post:post-create-list')
        payload = {'title': 'Test title', 'text': 'Test post text'}
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.access_token)
        response = self.client.post(url, data=payload)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
