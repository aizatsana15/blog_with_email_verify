import json

from django.http import JsonResponse

from rest_framework.status import HTTP_422_UNPROCESSABLE_ENTITY, HTTP_400_BAD_REQUEST

from dynamic_preferences.registries import global_preferences_registry

from core.utils import check_email_by_hunter


__all__ = [
    'CheckEmailMiddleware'
]


class CheckEmailMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if (
                request.path == '/auth/users/' and
                'application/json' in request.META['CONTENT_TYPE']
        ):

            data = json.loads(request.body)
            email = data.get('email')
            if not email:
                return JsonResponse({'result': 'Your must provide email address'}, status=HTTP_400_BAD_REQUEST)

            if global_preferences_registry.manager()['user_signup__allow_check_email']:
                result = check_email_by_hunter(data.get('email'))
                if not (
                        result and
                        result.get('score') >= global_preferences_registry.manager()['user_signup__hunter_allow_score']
                ):
                    return JsonResponse(
                        {'result': 'Your email address verifying failed'},
                        status=HTTP_422_UNPROCESSABLE_ENTITY
                    )

        response = self.get_response(request)
        return response
