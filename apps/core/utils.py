from django.conf import settings
from pyhunter import PyHunter


hunter = PyHunter(settings.PY_HUNTER_API_KEY)


__all__ = [
    'check_email_by_hunter'
]


def check_email_by_hunter(email):
    return hunter.email_verifier(email)
