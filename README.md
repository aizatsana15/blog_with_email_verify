## Сontents
* [General info](#general-info)
* [Setup](#setup)


## General info

Sunset is a blog that gives you ability register, create post, read and like posts another users.

You can find live site [here](https://sunsetblog.herokuapp.com/)

## Setup  
To run this project:
```
$ git clone https://gitlab.com/aizatsana15/sunsett.git
$ pip3 install -r requirements.txt
$ python3 manage.py migrate
```
