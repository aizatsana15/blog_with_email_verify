from dynamic_preferences.types import BooleanPreference, IntegerPreference
from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry

user_signup = Section('user_signup')


@global_preferences_registry.register
class AllowCheckUserEmail(BooleanPreference):
    section = user_signup
    name = 'allow_check_email'
    help_text = 'Allow or not to check user email via external services'
    default = True


@global_preferences_registry.register
class HunterAllowScore(IntegerPreference):
    section = user_signup
    name = 'hunter_allow_score'
    help_text = 'Minimum allow score to verify email address'
    default = 74
