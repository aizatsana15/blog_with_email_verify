from rest_framework import serializers
from post.models import Post

__all__ = [
    'PostSerializer',
    'LikeDislikeSerializer'
]


class PostSerializer(serializers.ModelSerializer):
    author = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Post
        fields = ('id', 'title', 'text', 'author', 'dt_created', 'total_likes', 'total_dislikes')


class LikeDislikeSerializer(serializers.Serializer):
    kind = serializers.IntegerField()
